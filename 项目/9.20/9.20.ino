#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMCore.h>

MeDCMotor motor_9(9);
MeDCMotor motor_10(10);
void move(int direction, int speed)
{
      int leftSpeed = 0;
      int rightSpeed = 0;
      if(direction == 1){
          leftSpeed = speed;
          rightSpeed = speed;
      }else if(direction == 2){
          leftSpeed = -speed;
          rightSpeed = -speed;
      }else if(direction == 3){
          leftSpeed = -speed;
          rightSpeed = speed;
      }else if(direction == 4){
          leftSpeed = speed;
          rightSpeed = -speed;
      }
      motor_9.run((9)==M1?-(leftSpeed):(leftSpeed));
      motor_10.run((10)==M1?-(rightSpeed):(rightSpeed));
}
double angle_rad = PI/180.0;
double angle_deg = 180.0/PI;
MeUltrasonicSensor ultrasonic_2(2);
MeLineFollower linefollower_1(1);

void setup(){
    while(!((ultrasonic_2.distanceCm()) < (10)))
    {
        _loop();
    }
}

void loop(){
    move(3,10);
    _delay(0.5);
    if((ultrasonic_2.distanceCm()) < (30)){
        move(4,10);
        _delay(0.5);
        if((ultrasonic_2.distanceCm()) > (10)){
            move(1,150);
            if(((linefollower_1.readSensors())==( 3 ))){
                move(2,100);
                _delay(1);
                move(3,100);
                _delay(0.5);
            }else{
                move(1,100);
            }
        }else{
            move(2,100);
            _delay(0.8);
            move(4,50);
            _delay(0.5);
            if(((linefollower_1.readSensors())==( 3 ))){
                move(2,150);
                _delay(1);
                move(3,100);
                _delay(0.5);
            }else{
                move(1,100);
            }
        }
        if(((linefollower_1.readSensors())==( 3 ))){
            move(2,100);
            _delay(1);
            move(3,100);
            _delay(0.5);
        }else{
            move(1,100);
        }
    }else{
        move(1,50);
        _delay(0.5);
        if((ultrasonic_2.distanceCm()) > (10)){
            move(1,150);
            if(((linefollower_1.readSensors())==( 3 ))){
                move(2,100);
            }else{
                move(1,100);
                _delay(1);
                move(3,100);
                _delay(0.5);
            }
        }else{
            move(2,100);
            _delay(0.8);
            move(4,50);
            _delay(0.5);
        }
    }
    _loop();
}

void _delay(float seconds){
    long endTime = millis() + seconds * 1000;
    while(millis() < endTime)_loop();
}

void _loop(){
}
